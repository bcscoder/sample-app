<aside id="logo-sidebar"
    class="fixed top-0 left-0 z-40 w-64 h-screen transition-transform -translate-x-full bg-white border-r border-gray-200 sm:translate-x-0 dark:bg-gray-800 dark:border-gray-700"
    aria-label="Sidebar">
    <div class="h-full pt-8 px-3 pb-4 overflow-y-auto bg-white dark:bg-gray-800">
        <div class="w-72 h-8 pl-6 pr-5 mb-8 flex-col justify-start items-start inline-flex">
            <div class="justify-start items-center gap-6 inline-flex">
                <div class="justify-start items-center gap-2 flex">
                    Admin Panel
                </div>
            </div>
        </div>
        <ul class="space-y-2 font-medium">
            <li>
                <a href="#"
                    class="flex items-center p-2 text-slate-600 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700 group">
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink"
                        width="24" height="24" x="0" y="0" viewBox="0 0 70 70"
                        style="enable-background:new 0 0 512 512" xml:space="preserve" class="">
                        <g>
                            <path
                                d="M62.934 2.395H7.066c-2.619 0-4.75 2.13-4.75 4.75v44.152c0 2.619 2.131 4.75 4.75 4.75H28.37c-.331 4.045-3.18 6.246-3.315 6.428h-2.596a4.383 4.383 0 0 0-4.38 4.38c0 .415.335.75.75.75H51.17a.75.75 0 0 0 .75-.75 4.386 4.386 0 0 0-4.38-4.38h-2.596c-.301-.403-2.959-2.075-3.315-6.428h21.305c2.619 0 4.75-2.131 4.75-4.75V42.79a.75.75 0 0 0-.75-.75H8.385a.75.75 0 1 0 0 1.5h57.799v7.756a3.254 3.254 0 0 1-3.25 3.25H7.066a3.254 3.254 0 0 1-3.25-3.25V7.145a3.254 3.254 0 0 1 3.25-3.25h55.868a3.254 3.254 0 0 1 3.25 3.25v30.779a.75.75 0 0 0 1.5 0V7.144c0-2.618-2.131-4.75-4.75-4.75zm-32.22 61.58h16.825c1.33 0 2.451.905 2.782 2.13H19.676a2.89 2.89 0 0 1 2.782-2.13h2.866c.238.01.449-.11.55-.207.275-.366 3.654-2.756 4.007-7.716h10.236a10.842 10.842 0 0 0 2.72 6.423H30.713a.75.75 0 1 0 0 1.5z"
                                fill="#000000" opacity="1" data-original="#000000"></path>
                            <path
                                d="M37.75 49.044c0-1.516-1.234-2.75-2.75-2.75s-2.75 1.234-2.75 2.75 1.233 2.75 2.75 2.75 2.75-1.234 2.75-2.75zm-4 0a1.251 1.251 0 1 1 1.25 1.25c-.69 0-1.25-.56-1.25-1.25zM10.031 10.128h18.102a.75.75 0 0 0 0-1.5H10.03a.75.75 0 0 0 0 1.5zM10.031 14.017h18.102a.75.75 0 1 0 0-1.5H10.03a.75.75 0 0 0 0 1.5zM50.796 28.793c5.338 0 9.923-4.318 9.923-9.917 0-5.47-4.45-9.92-9.92-9.92h-.003c-5.467 0-9.913 4.451-9.913 9.923 0 5.593 4.56 9.914 9.913 9.914zm5.064-3.199-3.712-5.968h7.038a8.357 8.357 0 0 1-3.326 5.968zm-4.312-15.105a8.435 8.435 0 0 1 7.637 7.637h-7.637zm-1.502 0v8.39c0 .136.073.332.113.397.062.1 4.723 7.588 4.428 7.112a8.462 8.462 0 0 1-3.791.905c-4.54 0-8.413-3.664-8.413-8.414 0-4.391 3.374-8.008 7.663-8.39zM10.031 38.674h32a.75.75 0 0 0 0-1.5h-1.645v-3.868a2.753 2.753 0 0 0-2.75-2.75h-4.052v-.515a2.753 2.753 0 0 0-2.75-2.75H26.78v-.173a2.753 2.753 0 0 0-2.75-2.75H19.98v-1.153a2.753 2.753 0 0 0-2.75-2.75h-2.802a2.753 2.753 0 0 0-2.75 2.75v13.959H10.03a.75.75 0 0 0 0 1.5zm27.605-6.618c.69 0 1.25.561 1.25 1.25v3.868h-5.302v-5.118zm-6.802-3.265c.69 0 1.25.56 1.25 1.25v7.133H26.78V28.79zm-6.803-2.923c.69 0 1.25.561 1.25 1.25v10.056H19.98V25.868zm-10.854-2.653c0-.689.56-1.25 1.25-1.25h2.802c.69 0 1.25.561 1.25 1.25v13.959h-5.302z"
                                fill="#000000" opacity="1" data-original="#000000"></path>
                        </g>
                    </svg>
                    <span class="ms-3 text-sm font-medium">Dashboard</span>
                </a>
            </li>
         
           
            <li>
                <h5
                    class="p-2 mb-2 text-sm font-light tracking-wide text-gray-900 uppercase lg:text-xs dark:text-white">
                    Master Data</h5>
            </li>

            <li>
                <router-link to="/admin/blogs"
                    class="flex items-center p-2 text-slate-600 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700 group">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"
                        fill="currentColor">
                        <path
                            d="M20 22H4C3.44772 22 3 21.5523 3 21V3C3 2.44772 3.44772 2 4 2H20C20.5523 2 21 2.44772 21 3V21C21 21.5523 20.5523 22 20 22ZM19 20V4H5V20H19ZM7 6H11V10H7V6ZM7 12H17V14H7V12ZM7 16H17V18H7V16ZM13 7H17V9H13V7Z">
                        </path>
                    </svg>
                    <span class="ms-3 text-sm font-medium">Blogs</span>
                </router-link>
            </li>




        </ul>
    </div>
</aside>
