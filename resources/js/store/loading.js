// store/index.js
import { defineStore } from 'pinia';

export const useLoadingStore = defineStore({
    id: 'loading',
    state: () => ({
        loading: false,
    }),
    getters: {
        getLoading: (state) => {
            return state.loading;
        },
    },
    actions: {
        setLoading(loading) {
            this.loading = loading;
        },
    }
});
