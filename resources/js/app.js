import './bootstrap';
import { createApp } from "vue/dist/vue.esm-bundler";
import { createPinia } from 'pinia'
const pinia = createPinia()
import { plugin, defaultConfig } from '@formkit/vue'

// 1. Define route components.
// These can be imported from other files
import DeleteConfirmation from "./components/DeleteConfirmation.vue";
import Loading from "./components/Loading.vue";
import router from './router'

const app = createApp({})
app.use(plugin, defaultConfig)
app.use(router)
app.use(pinia)
app.component('DeleteConfirmation', DeleteConfirmation);
app.component('Loading', Loading);

app.mount('#app')

import { useLoadingStore } from './store/loading';

axios.interceptors.request.use(config => {
    const { setLoading } = useLoadingStore();
    setLoading(true);
    return config;
});

axios.interceptors.response.use(
    response => {
        const { setLoading } = useLoadingStore();
        setLoading(false);
        return response;
    },
    error => {
        const { setLoading } = useLoadingStore();
        setLoading(false);
        return Promise.reject(error);
    }
);



// Now the app has started!

