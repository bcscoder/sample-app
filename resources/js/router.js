import './bootstrap';
import { createApp } from "vue/dist/vue.esm-bundler";
import * as VueRouter from 'vue-router';
import { createPinia } from 'pinia'
const pinia = createPinia()

// 1. Define route components.
// These can be imported from other files
import Dashboard from "./components/Dashboard.vue";
import BlogList from "./components/BlogList.vue";
// 2. Define some routes
// Each route should map to a component.
// We'll talk about nested routes later.
const routes = [
    { path: '/admin/dashboard', component: Dashboard },
    { path: '/admin/blogs', component: BlogList },

]

const router = VueRouter.createRouter({
    history: VueRouter.createWebHistory(),
    routes, // short for `routes: routes`
})

export default router;
