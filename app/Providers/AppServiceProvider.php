<?php

namespace App\Providers;

use App\Models\Blog;
use App\Repositories\BlogRepository;
use App\Repositories\Cache\CacheBlogDecorator;
use App\Repositories\Eloquent\EloquentBlogRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
