<?php

namespace App\Providers;

use App\Models\Blog;
use App\Repositories\BlogRepository;
use App\Repositories\Cache\CacheBlogDecorator;
use App\Repositories\Eloquent\EloquentBlogRepository;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BlogServiceProvider extends ServiceProvider
{
    protected string $moduleName = 'Blog';

    protected string $moduleNameLower = 'blog';

   

    /**
     * Register the service provider.
     */
    public function register(): void
    {
        $this->app->bind(BlogRepository::class,
            function () {
                $repository = new EloquentBlogRepository(new Blog);
                if (!config('app.cache')) {
                    return $repository;
                }

                return new CacheBlogDecorator($repository);
            }
        );
    }
}
