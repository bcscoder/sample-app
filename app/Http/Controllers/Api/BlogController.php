<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateBlogRequest;
use App\Http\Requests\UpdateBlogRequest;
use App\Http\Resources\BlogResource;
use App\Repositories\BlogRepository;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(BlogRepository $blogRepository)
    {
        $blogs = $blogRepository->paginate(request('limit', 15));

        return BlogResource::collection($blogs);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateBlogRequest $request, BlogRepository $blogRepository)
    {
        $blog = $blogRepository->create($request->all());

        return response()->json([
            'message' => 'Blog successfully created',
            'data' => new BlogResource($blog),
        ], 201);
    }

    /**
     * Show the specified resource.
     */
    public function show($id, BlogRepository $blogRepository)
    {
        $blog = $blogRepository->find($id);

        return new BlogResource($blog);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateBlogRequest $request, $id, BlogRepository $blogRepository)
    {
        $blog = $blogRepository->find($id);

        $updated = $blogRepository->update($blog, $request->all());

        return response()->json([
            'message' => 'Blog successfully updated',
            'data' => new BlogResource($updated),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id, BlogRepository $blogRepository)
    {
        $blog = $blogRepository->find($id);
        $blogRepository->destroy($blog);

        return response()->json([
            'message' => 'Blog successfully deleted.'
        ]);
    }
}
