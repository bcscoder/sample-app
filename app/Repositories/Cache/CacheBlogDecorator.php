<?php

namespace App\Repositories\Cache;
use App\Repositories\BlogRepository;
use App\Repositories\Cache\BaseCacheDecorator;


class CacheBlogDecorator extends BaseCacheDecorator implements BlogRepository
{
    public function __construct(BlogRepository $blogRepository)
    {
        parent::__construct();
        $this->entityName = 'blog.blog';
        $this->repository = $blogRepository;
    }
}
