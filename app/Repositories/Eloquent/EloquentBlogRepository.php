<?php

namespace App\Repositories\Eloquent;
use App\Repositories\BlogRepository;
use App\Repositories\Eloquent\EloquentBaseRepository;

class EloquentBlogRepository extends EloquentBaseRepository implements BlogRepository{

    public function paginate($perPage = 15)
    {
        if (method_exists($this->model, 'translations')) {
            return $this->model->with('translations')->orderBy('created_at', 'DESC')->paginate($perPage);
        }
        $model = $this->model;
        
        if(request('with')){
            $model = $model->with(request('with'));
        }

        return $model->orderBy('created_at', 'DESC')->paginate($perPage);
    }
}
