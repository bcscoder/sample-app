<?php

namespace Tests\Unit;

use App\Repositories\BlogRepository;
use PHPUnit\Framework\TestCase;

class BlogRepositoryTest extends TestCase
{
    private $blogRepository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->blogRepository = app(BlogRepository::class);
    }

     /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCreateBlog()
    {
        $blog = $this->blogRepository->create([
            'title' => 'Test Blog',
            'slug' => 'test-blog',
            'content' => 'This is a test blog content.',
            'category_id' => '1', // Replace with your desired category ID
        ]);

        $this->assertInstanceOf(Blog::class, $blog);
        $this->assertCount(1, $this->blogRepository->all());
    }

    public function testEditBlog()
    {
        $blog = $this->blogRepository->create([
            'title' => 'Test Blog',
            'slug' => 'test-blog',
            'content' => 'This is a test blog content.',
        ]);

        $this->blogRepository->update($blog, [
            'title' => 'Updated Test Blog',
            'slug' => 'updated-test-blog',
            'content' => 'This is an updated test blog content.',
        ]);

        $updatedBlog = $this->blogRepository->find($blog->id);

        $this->assertEquals('Updated Test Blog', $updatedBlog->title);
        $this->assertEquals('updated-test-blog', $updatedBlog->slug);
        $this->assertEquals('This is an updated test blog content.', $updatedBlog->content);
        $this->assertEquals('2', $updatedBlog->category_id);
    }

    public function testDeleteBlog()
    {
        $blog = $this->blogRepository->create([
            'title' => 'Test Blog',
            'slug' => 'test-blog',
            'content' => 'This is a test blog content.',
        ]);

        $this->blogRepository->destroy($blog);

        $this->assertEquals(0, $this->blogRepository->all()->count());
    }
}
